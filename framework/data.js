const pages = { google_homepage_url : 'https://www.google.com/' };
const selectors = { google_search_input : '#lst-ib',
                    google_search_submit: '#tsf > div.tsf-p > div.jsb > center > input[type="submit"]:nth-child(1)',
                    num_links: '#resultStats' };

module.exports = { pages,
                   selectors };
