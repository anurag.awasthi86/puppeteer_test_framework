const puppeteer = require('puppeteer');

async function init() {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    const context = { browser, page };
    return context;
}

async function debug_init() {
    const browser = await puppeteer.launch({
        headless: false,
        slowMo: 50
    });
    const page = await browser.newPage();
    const context = { browser, page };
    return context;
}

async function exit(context) {
    await context.browser.close();
}

async function capture_screenshot(context, filename) {
    await context.page.screenshot({path: `output/screenshot/${filename}.png`});
}

async function capture_pdf(context, filename, format) {
    await context.page.pdf({path: `output/pdf/${filename}.pdf`, format: `${format}`});
}

async function start_tracing(context, filename, screenshots) {
    await context.page.tracing.start({path: `output/trace/${filename}.json`, screenshots: screenshots});
}

async function stop_tracing(context) {
    await context.page.tracing.stop();
}

async function goto(context, url) {
    await context.page.goto(url);
}

function current_url(context) {
    return context.page.url();
}

async function type(context, selector, term) {
    await context.page.type(selector, term);
}

async function click(context, selector) {
    await context.page.focus(selector);
    await context.page.click(selector);
    await context.page.waitForNavigation();
}

async function get_inner_html(context, selector) {
    const result = await context.page.$eval(selector, element => element.innerHTML);
    return result;   
}

module.exports = {
    init,
    debug_init,
    exit,
    capture_screenshot,
    capture_pdf,
    start_tracing,
    stop_tracing,
    goto,
    current_url,
    type,
    click,
    get_inner_html, };