const { Given, When, Then, Before, After, setDefaultTimeout } = require('cucumber');
const { expect } = require('chai');
const framework = require('../../framework/index');
const { pages, selectors } = require('../../framework/data');

Before({tags: "@headless"}, async function () {
  this.context = await framework.init();
});

Before({tags: "@ui"}, async function () {
  setDefaultTimeout(10 * 1000);
  this.context = await framework.debug_init();
});

After(async function () {
  await framework.exit(this.context);
});

Given('I am on Google homepage', async function () {
  const google_homepage_url = pages.google_homepage_url;
  await framework.goto(this.context, google_homepage_url);
  const url = framework.current_url(this.context);
  expect(url).to.eql(google_homepage_url);
});

When('I search for {string}', async function (search_term) {
  await framework.type(this.context, selectors.google_search_input, search_term);
  await framework.click(this.context, selectors.google_search_submit);
});

Then('the number of links present should be {int}', async function (expected_num_links) {
  let content = await framework.get_inner_html(this.context, selectors.num_links);
  let num_links = parseInt(content.slice(6,18).replace(/,/g, ''));
  expect(num_links).to.eql(expected_num_links);
});