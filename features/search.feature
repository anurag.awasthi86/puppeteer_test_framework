Feature: Search
  In order to test search results for a term
  As a user
  I want to search for the term and validate number of links as well as print nth linktext if present
  
  @headless
  Scenario: Search for Aviva in a headless browser
    Given I am on Google homepage
    When I search for "Aviva"
    Then the number of links present should be 39800000

  @ui
  Scenario: Search for Aviva in a browser with UI
    Given I am on Google homepage
    When I search for "Aviva"
    Then the number of links present should be 28800000

  